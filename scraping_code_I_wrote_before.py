from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from bs4 import BeautifulSoup

import time
import requests
import shutil
from mimetypes import guess_extension
import os.path
import csv
import sys
from spylls.hunspell import Dictionary
from wordfreq import word_frequency, zipf_frequency
from spellchecker import SpellChecker
import re


imgSelector = 'li[data-csa-c-action="image-block-main-image-hover"] div#imgTagWrapperId img'
# priceSelector = 'div#corePrice_desktop span.a-price.a-text-price span[aria-hidden="true"]' # doesn't work
priceSelector = 'span.a-offscreen'
nameSelector = 'div#ppd div#title_feature_div div#titleSection span#productTitle'

def main():
    # with open('scrapeResults.csv') as csvfile:
    #     reader = csv.reader(csvfile)
    #     rows = [row for row in reader]
    # names = [row[2] for row in rows]
    # #print(names)
    # cleanNames = [cleanName(i) for i in names]
    # print(cleanNames)
    # sys.exit()

    simpleList = False
    if simpleList:
        urlList = openUrlList("amazon_url_samples.csv")
    else:
        urlList = openWishLists("wishlists_sample.csv")

    web = 'https://www.amazon.com'

    options = webdriver.ChromeOptions()
    #options.add_argument('--headless')
    driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
    driver.get(web)
    # now we wait up to 30 seconds for the search text box to appear. in the meanwhile you have to manually do the CAPTCHA
    element = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.CSS_SELECTOR, "input#twotabsearchtextbox")))
    
    # now we should be good to go, logged in or whatever
    table = urlListToInfoTable(urlList, driver)
    
    # save it to a csv in the top level
    with open('scrapeResults3.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(table)
        
    driver.quit()
    ########## end main()



# def existsElementBySelector(selector, driver):
#     found = driver.find_elements(By.CSS_SELECTOR, selector)
#     return len(found)

def openWishLists(path):
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        header = next(reader) # the first row is headers
        wishList = [row for row in reader]
    # now get it into long format
    print(wishList)
    itemList = []
    for row in wishList:
        id = row.pop(0)
        platformId = row.pop(0)
        for i, url in enumerate(row):
            itemList.append([id, platformId, i+1, url])
    # now itemList should be the long format
    return itemList

def openUrlList(path):
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        urlList = [row[0] for row in reader]
    return urlList

    
    
def urlListToInfoTable(urlList, driver):
    #table = [['Amazon URL', 'Image Path', 'Product Name', 'Price']]
    table = [['ID', 'PlatformID', 'WishlistNumber', 'Name', 'Price', 'Image Path', 'Amazon URL', 'Raw Product Name']]
    id = 'NA'
    platformId = 'NA'
    wishListNumber = '0'
    for item in urlList:
        if isinstance(item, str):
            url = item
        elif isinstance(item, list):
            id = item[0]
            platformId = item[1]
            wishListNumber = item[2]
            url = item[-1] # it's the last column in the table if it's the long form wishlist
        info = scrapeUrl(url, driver, id, wishListNumber)
        table.append([id, platformId, wishListNumber, info['cleanName'], info['productPrice'], info['imagePath'], info['amazonUrl'], info['productName']])
    return table

def scrapeUrl(url, driver, id=None, wishListNumber=None):
    print(url)
    driver.get(url)
    # start by waiting for the image element
    imageElem = waitForSelector(driver, imgSelector)
    # save the image element
    imagePath = saveImageFromElem(imageElem, id, wishListNumber)
    # get the raw name
    rawName = getName_fgp(driver, nameSelector)
    # get the cleaned name
    cname = cleanName(rawName)
    # get the price
    price = getPrice_fgp(driver, priceSelector)
    # get the page source. by now it should be pretty well loaded haha
    pageSource = driver.page_source
    # save the page source
    srcPath = savePageSource(pageSource, id, wishListNumber)
    print(f'{price} for {cname}')
    return {'cleanName':cname, 'productPrice':price, 'imagePath':imagePath, 'amazonUrl': url, 'productName':rawName}


def waitForSelector(driver, selector):
    elem = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
    # now we believe the page is loaded so get the page
    return elem

def saveImageFromElem(imgElem, id, wishListNum):
    # note this function will error if you call it with an inappropriate element
    imgSrc = imgElem.get_attribute('src')
    fpath = saveImgUrl(imgSrc, id, wishListNum)
    return fpath

def getImage_fgp(driver):
    elem = driver.find_element(By.CSS_SELECTOR, imgSelector)
    imgSrc = elem.get_attribute('src')
    fpath = saveImgUrl(imgSrc)
    return fpath

def getName_fgp(driver, nameSelector):
    elem = driver.find_element(By.CSS_SELECTOR, nameSelector)
    name = elem.get_attribute('innerText')
    return name

def getPrice_fgp(driver, priceSelector):
    elem = driver.find_element(By.CSS_SELECTOR, priceSelector)
    price = elem.get_attribute('innerText')
    return price
    
    
def saveImgUrl(url, id, wishListNum, asset_dir='images', exts=[".png", ".jpeg", ".jpg", ".svg", ".gif", ".pdf", ".ico"]):
    response = requests.get(url, stream=True)
    ext = guess_extension(response.headers['Content-Type'].split(';')[0].strip())
    if ext is None or ext not in exts:
            #Don't know the file extension, or not in the whitelist
            return None
    # make an appropriate name
    if id and (wishListNum is not None):
        fname = str(id) + '_' + str(wishListNum)
    elif id:
        fname = str(id)
    elif wishListNum is not None:
        fname = 'wishListNum_' + str(wishListNum)
    else:
        fname = 'untitled'
    fname = fname + '_productImage'        
        
    # get numbered default name
    for i in range(1000):
        idx = i + 1
        fpath = f"{fname}_{idx:03d}{ext}"
        fpath = os.path.join(asset_dir, fpath)
        if not os.path.exists(fpath): break
    if idx == 1000:
        print("Too many files with same name! Ugh! We are losing some!")
            
    with open(fpath, 'wb') as outFile:
        shutil.copyfileobj(response.raw, outFile)
    del response
    return fpath


def savePageSource(pageSource, id, wishListNum, asset_dir='sources', ext=".html"):
    if id and (wishListNum is not None):
        fname = str(id) + '_' + str(wishListNum)
    elif id:
        fname = str(id)
    elif wishListNum is not None:
        fname = 'wishListNum_' + str(wishListNum)
    else:
        fname = 'untitled'
    fname = fname + '_pgSrc'
    # get numbered file name
    for i in range(1000):
        idx = i + 1
        fpath = f"{fname}_{idx:03d}{ext}"
        fpath = os.path.join(asset_dir, fpath)
        if not os.path.exists(fpath): break
    if idx == 1000:
        print("Too many files with same name! Ugh! We are losing some!")
    
    with open(fpath, 'w') as outFile:
        outFile.write(pageSource)
    return fpath


def cleanName(name):
    includes = ['rgb', '3d']
    units = ['piece', 'degree', 'mm', 'cm', 'in', 'lb', 'lbs', 'kg', 'm', 'foot', 'ft', 'w']
    spell = SpellChecker()
    words = name.split()
    words = splitListAlNum(words)
    useWords = [i.strip('.,;"-#() ') for i in words]
    outWords = []
    outFreqs = []
    knownUseWords = spell.known(useWords)
    zipfFreqUseWords = [zipf_frequency(i, 'en') for i in useWords]
    
    for i, word in enumerate(words):
        # first check for number/unit matches
        if i>0 and is_number(useWords[i-1]) and (useWords[i].lower() in units):
            #print("Found number unit match!")
            outWords.append(useWords[i-1] + '_' + useWords[i])
            continue
        # now test the individual word
        include = (useWords[i].lower() in knownUseWords)
        include = (include or useWords[i] in includes) # manually added extra words we want to keep
        include = include and (zipfFreqUseWords[i] < 6.5)
        include = include and (len(useWords[i]) > 1)
        if include:
            outWords.append(useWords[i])
            outFreqs.append(zipf_frequency(word, 'en'))
        else:
            continue
    # now we have all the "real" words, should be no proper names or garbage
    #print(outWords)
    #print(outFreqs)
    outWordsShort = outWords[0:7]
    return ' '.join(outWordsShort)


def splitListAlNum(wordList):
    outWords = []
    for w in wordList:
        outWords.extend(splitAlNum(w))
    return outWords

def splitAlNum(word):
    # splits apart something like 0.1mm into 0.1 mm
    #a = re.search("^\d+", word)
    a = re.search("^[0-9.]+", word)
    #b = re.search("\w+$", word)
    b = re.search("[^\d\W]+$", word)
    if a and b:
        a = a.group(0)
        b = b.group(0)
        return [a, b]
    else:
        return [word]
    


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    
    
if __name__ == "__main__":
    main()
    
# The Amazon CAPTCHA page has the following comment code at the beginning:
# <!--
#         To discuss automated access to Amazon data please contact api-services-support@amazon.com.
#         For information about migrating to our APIs refer to our Marketplace APIs at https://developer.amazonservices.com/ref=rm_c_sv,
#         or our Product Advertising API at https://affiliate-program.amazon.com/gp/advertising/api/detail/main.html/ref=rm_c_ac
#         for advertising use cases.
# -->

# The CAPTCHA itself seems to be in a form like 
# <form method="get" action="/errors/validateCaptcha" name="">
#    <input type="hidden" name="amzn" value="E/nv+XgF2UyTrgiQbuomRg=="><input type="hidden" name="amzn-r" value="/">
#    <div class="a-row a-spacing-large">
#        <div class="a-box">
#            <div class="a-box-inner">
#                <h4>Type the characters you see in this image:</h4>
